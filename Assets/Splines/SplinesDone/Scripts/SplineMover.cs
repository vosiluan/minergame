﻿using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using static GlobalEnum;
public class SplineMover : MonoBehaviour {
    public SplineDone spline;
    public float speed;
    private float moveAmount;
    private float maxMoveAmount;
    IEnumerator actionMove;
    public enum TypeMove { 
        GoForward,
        GoBack
    }
    [Button] void TestGoForward() {
        StartMove(TypeMove.GoForward);
    }
    [Button] void TestGoBack(){
        StartMove(TypeMove.GoBack);
    }
    [Button] void TestStopMove(){
        StopMove();
    }
    private void Start(){
        maxMoveAmount = spline.GetSplineLength();
    }
    void SetVectorMove() {
        Vector3 _vtMove = new Vector3(spline.GetPositionAtUnits(moveAmount).x, transform.position.y, spline.GetPositionAtUnits(moveAmount).z);
        transform.position = spline.GetPositionAtUnits(moveAmount);
        transform.position = _vtMove;
        transform.forward = spline.GetForwardAtUnits(moveAmount);
    }
    IEnumerator Goforward()
    {
        while (moveAmount < maxMoveAmount)
        {
            moveAmount = (moveAmount + (Time.deltaTime * speed));
            if (moveAmount >=maxMoveAmount) {
                break;
            }
            SetVectorMove();
            yield return null;
        }
        StopMove();
    }
    IEnumerator GoBack()
    {
        while (moveAmount > 0)
        {
            moveAmount = (moveAmount - (Time.deltaTime * speed));
            if (moveAmount <= 0){
                break;
            }
            SetVectorMove();
            yield return null;
        }
        StopMove();
    }
    public void StartMove(TypeMove typeMove) {
        if (actionMove != null) {
            StopCoroutine(actionMove);
            actionMove = null;
        }
        if(typeMove == TypeMove.GoForward)
            actionMove = Goforward();
        else
            actionMove = GoBack();
        StartCoroutine(actionMove);
    }
    public void StopMove() {
        if (actionMove != null)
        {
            StopCoroutine(actionMove);
            actionMove = null;
        }
    }
}
