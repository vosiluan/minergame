using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalEnum;
public class MinerGoldController : MonoBehaviour
{
    public List<GameObject> goldCargoSlots;
    public List<StackCargoController> goldCargoSlotSpaneds;
    public int MaxCapicity;
    public int currentCapicity = 0;
    public StackCargoController cargoCopperPrefap;
    public StackCargoController cargoDiamondPrefap;
    public StackCargoController cargoGoldPrefap;
    // Start is called before the first frame update
    void Start()
    {
        maxText.SetActive(false);
    }
    [Button("Sell")]
    public void Sell()
    {
        StartCoroutine(SellGold());
    }
    IEnumerator SellGold()
    {
        int _totalMoney = 0;
        for (int  i  = goldCargoSlotSpaneds.Count-1; i >= 0; i--)
        {
            goldCargoSlotSpaneds[i].transform.parent = null;
            LeanTween.move(goldCargoSlotSpaneds[i].gameObject, SellPosition.position, 1);
            _totalMoney += goldCargoSlotSpaneds[i].priceSell;
            yield return new WaitForSeconds(0.1f);
            SellGoldDone(i);
            Destroy(goldCargoSlotSpaneds[i].gameObject);
        }
        maxText.gameObject.SetActive(false);
        GameController.instance.boxSellController.gameObject.GetComponent<SellCener>().PushGold(_totalMoney);
        yield return new WaitForSeconds(2);
        goldCargoSlotSpaneds.Clear(); 
        currentCapicity = 0;
    }
    public void SellGoldDone(int _index) {
        Debug.Log("SellGold Done");
        GameController.instance.boxSellController.CreateMoney(_index);
    }
    public Transform SellPosition;
    [Button("Add")]
    public void Add(TypeStone _typeStone)
    {
        if (currentCapicity < MaxCapicity)
        {
            StackCargoController newSlot;
            switch (_typeStone) {
                case TypeStone.Copper:
                    newSlot = Instantiate(cargoCopperPrefap, goldCargoSlots[currentCapicity].transform);
                    goldCargoSlotSpaneds.Add(newSlot);
                    break;
                case TypeStone.Diamond:
                    newSlot = Instantiate(cargoDiamondPrefap, goldCargoSlots[currentCapicity].transform);
                    goldCargoSlotSpaneds.Add(newSlot);
                    break;
                case TypeStone.Gold:
                    newSlot = Instantiate(cargoGoldPrefap, goldCargoSlots[currentCapicity].transform);
                    goldCargoSlotSpaneds.Add(newSlot);
                    break;
            }
            currentCapicity++;
            if (IsMax()) maxText.SetActive(true);
        }
    }
    public GameObject maxText;
    public bool IsMax()
    {
        return currentCapicity >= MaxCapicity;
    }
    public void ResetPosStack()
    {
        for (int i = 0; i < goldCargoSlotSpaneds.Count;i++)
        {
            goldCargoSlotSpaneds[i].transform.position = goldCargoSlots[i].transform.position;
        }
        currentCapicity = goldCargoSlotSpaneds.Count;
        maxText.SetActive(false);
    }
    void Update()
    {
        maxText.transform.LookAt(Camera.main.transform);
    }
}
