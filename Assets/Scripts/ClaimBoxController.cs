using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClaimBoxController : MonoBehaviour
{
    public SellCener center; 
    private void OnTriggerEnter(Collider other)
    {
        center.Claim();
        if (GameController.instance.cartController.isFull) {
            GameController.instance.cartController.ResetCart();
        }
    }
}
