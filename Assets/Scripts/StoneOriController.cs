using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StoneOriController : MonoBehaviour
{
    [SerializeField] StoneController currentStone;
    [SerializeField] StoneController stonePrefab;
    [SerializeField] QuarryController myQuarry;
    public bool isCanMine;
    void OnEnable(){
        isCanMine = true;
    }
    public IEnumerator ResetStone(float _timewait) {
        yield return new WaitForSeconds(_timewait);
        StoneController _stone = Instantiate(stonePrefab,transform);
        _stone.InitStone();
        _stone.transform.localPosition = new Vector3();
        _stone.gameObject.transform.localScale = Vector3.zero;
        LeanTween.scale(_stone.gameObject,Vector3.one,0.5f).setOnComplete(()=> {
            currentStone = _stone;
            isCanMine = true;
            myQuarry.isCanMine = true;
        }); 
    }
    public void Mine(float _timeWaitReset) {
        currentStone.MineStone();
        currentStone = null;
        isCanMine = false;
        StartCoroutine(ResetStone(_timeWaitReset));
    }
}
