using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SellCener : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject moneyIconPrefap;
    public  void PushGold(int gold)
    {
        totalMoney += gold;
    }
    public int totalMoney = 0;
    bool Started = false;
    [Button("Claim")]
    public void Claim()
    {
        if (totalMoney <= 0) return;
        if (adding) return;
        adding = true;
        StartCoroutine(ClaimAnimated());
    }
    IEnumerator ClaimAnimated()
    {
        List<GameObject> spawend = new List<GameObject>();
        GameController.instance.boxSellController.ClearMoney();
        for (int i = 0; i < 20; i++)
        {
            GameObject newIcon = Instantiate(moneyIconPrefap, moneySpawnPoint.position, moneyIconPrefap.transform.rotation);
            newIcon.transform.parent = moneySpawnPoint.transform.parent;
            LeanTween.move(newIcon.gameObject, moneySpawnPoint.position + new Vector3(Random.Range(-200, 200), Random.Range(-200, 200), 0), 0.5f);
            spawend.Add(newIcon);
        }
        yield return new WaitForSeconds(0.5f);
        foreach (var i in spawend)
        {
            GameObject target = i;
            LeanTween.move(i.gameObject, tagetPoint.position,1).setOnComplete(()=> { Destroy(i); });
        }
        yield return new WaitForSeconds(1);
        GameController.instance.MoneyEffect(GameController.instance.player.myMoney+totalMoney);
        totalMoney = 0;
        adding = false;
    }
    bool adding = false;
    public Transform moneySpawnPoint, tagetPoint;

}
