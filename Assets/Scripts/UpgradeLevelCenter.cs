using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalEnum;
public class UpgradeLevelCenter : MonoBehaviour
{
    [SerializeField] List<LevelInfo> listLevelMoveSpeed;
    [SerializeField] List<LevelInfo> listLevelMineSpeed;
    [SerializeField] List<LevelInfo> listLevelStackStone;
    public int levelMaxNumber = 10;
    public int levelMinNumber = 1;
    public LevelInfo GetLevelInfoMove(TypeUpgrade _typeUpgrade ,int _index) {
        LevelInfo _level = new LevelInfo();
        switch (_typeUpgrade) {
            case TypeUpgrade.MineSpeed:
                _level = listLevelMineSpeed[_index-1];
                break;
            case TypeUpgrade.MoveSpeed:
                _level = listLevelMoveSpeed[_index-1];
                break;
            case TypeUpgrade.StackStone:
                _level = listLevelStackStone[_index-1];
                break;
        }
        return _level;
    }
    [System.Serializable]
    public class LevelInfo {
        public string levelName;
        public int levelNumber;
        public float levelValue;
        public int moneyUnlockLevelNext;
    }
    public bool CheckLevel(int _level) {
        return (_level >= levelMinNumber && _level <= levelMaxNumber) ? true : false;
    }
}
