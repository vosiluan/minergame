using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalEnum : MonoBehaviour
{
    public enum RoundBoxType { 
        UnlockMap,
        UnlockQuarry,
        UnlockCart,
        Sell,
        Upgrade,
    }
    public enum TypeUpgrade { 
        MoveSpeed,
        MineSpeed,
        StackStone
    }
    public enum TypeStone { 
        Copper,
        Diamond,
        Gold
    }
}
