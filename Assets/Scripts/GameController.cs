using EazyCamera;
using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalEnum;
using static UpgradeLevelCenter;
public class GameController : MonoBehaviour
{
    public static GameController instance;
    public UpgradePanelController upgradePanel;
    public MinerController miner;
    public GameObject map2;
    public GameObject wallMap2;
    public RoundBox_Controller boxSellController;
    public RoundBox_Controller boxUnlockMapController;
    public PlayerGroup player;
    public ParticleSystem mineEffect;
    public List<RoundBox_Controller> listRoundBox;
    public CartController cartController;
    public Transform upgradeCameraTarget;
    public Transform defaultCameraTarget;
    public Transform roundBoxCameraTarget;
    public EazyCam camera;
    IEnumerator myGoldCorotine;
    public int countUnlockRoundBox;
    [Button] void TestCamera() {
        StartCoroutine(AnimCameraUnlock(upgradeCameraTarget));
    }
    [System.Serializable]
    public class PlayerGroup{
        public TMPro.TextMeshProUGUI txtMoney;
        public long myMoney;
        public void SetTextMoney() {
            txtMoney.text = MyConstant.FormatToCurrency(myMoney,999);
        }
    }
    void Awake(){
        instance = this;
    }
    void Start() {
        countUnlockRoundBox = 0;
        LoadDataPlayer();
        player.txtMoney.text = MyConstant.FormatToCurrency(player.myMoney, 999);
    }
    public void IsShowPanelUpgrade(bool _isShow) {
        upgradePanel.gameObject.SetActive(_isShow);
    }
    public void UnlockMap2() {
        map2.SetActive(true);
        wallMap2.SetActive(false);
    }
    public void MoneyEffect(long _moneyUpdate)
    {
        if (myGoldCorotine != null)
        {
            StopCoroutine(myGoldCorotine);
        }
        myGoldCorotine = MyConstant.TweenValue(player.myMoney, _moneyUpdate, 10,
            (_valueUpdate) => {
                player.txtMoney.text = MyConstant.FormatToCurrency(_valueUpdate, 999);
            }, (_valueFinish) => {
                player.txtMoney.text = MyConstant.FormatToCurrency(_valueFinish, 999);
                player.myMoney = _moneyUpdate;
                DataManager.instance.userdata.money = player.myMoney;
                myGoldCorotine = null;
            });
        StartCoroutine(myGoldCorotine);
    }
    public void LoadDataPlayer() {
        player.myMoney = DataManager.instance.userdata.money;
        DataManager.UserData _userData = DataManager.instance.userdata;
        if (_userData.isUnlockMap2) {
            UnlockMap2();
            boxUnlockMapController.UnlockWhenLoadData();
        }
        if (listRoundBox.Count>0) {
            for (int i = 0; i < _userData.unlockedBoxCount;i++) {
                listRoundBox[i].UnlockWhenLoadData();
            }
        }
        LevelInfo _levelMove = upgradePanel.upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.MoveSpeed,_userData.levelMoveSpeed);
        LevelInfo _levelMine = upgradePanel.upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.MineSpeed,_userData.levelMineSpeed);
        LevelInfo _levelStack = upgradePanel.upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.StackStone,_userData.levelStackStone);
        miner.GetComponent<MinerGoldController>().MaxCapicity = (int)_levelStack.levelValue;
        miner._moveSpeed = _levelMove.levelValue;
        miner._mineSpeed = _levelMine.levelValue;
        upgradePanel.LoadData();
    }
    IEnumerator AnimCameraUnlock(Transform _target) {
        miner._joystick.gameObject.SetActive(false);
        camera._followTarget = _target;
        miner._joystick.SetDefault();
        miner.StopMove();
        yield return new WaitForSeconds(2f);
        miner._joystick.gameObject.SetActive(true);
        camera._followTarget = defaultCameraTarget;
    }
    public void StartAnimCameraToUpgradeBox() {
        StartCoroutine(AnimCameraUnlock(upgradeCameraTarget));
    }
    public void StartAnimCameraToQuarry4()
    {
        StartCoroutine(AnimCameraUnlock(roundBoxCameraTarget));
    }
}
