using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeMineController : MonoBehaviour
{
    [SerializeField] MinerController miner;
    [SerializeField] float speedMine;
    void OnTriggerEnter(Collider other){
        if (other.GetComponent<QuarryController>() != null)
            miner.SetDroper(other.GetComponent<QuarryController>());
        if (other.GetComponent<CartController>() != null) {
            StartCoroutine(other.GetComponent<CartController>().Add(miner.GetComponent<MinerGoldController>().goldCargoSlotSpaneds));
        }
    }
    void OnTriggerExit(Collider other){
        if (other.GetComponent<QuarryController>() != null)
            miner.SetDroper(null);
    }
}
