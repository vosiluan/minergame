using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QuarryController : MonoBehaviour
{
    public List<StoneOriController> listOriStone;
    [SerializeField] float timeWaitResetStone;
    public bool isCanMine;
    void OnEnable(){
        isCanMine = true;
    }
    public void MineStone() {
        if (!isCanMine) return;
        for (int i = 0; i < listOriStone.Count;i++) {
            if (listOriStone[i].isCanMine) {
                listOriStone[i].Mine(timeWaitResetStone);
                GameController.instance.mineEffect.transform.position = listOriStone[i].transform.position;
                GameController.instance.mineEffect.Play();
                break;
            }
        }
        isCanMine = CheckIsCanMine();
    }
    public bool CheckIsCanMine() {
        for (int i = 0; i < listOriStone.Count; i++)
        {
            if (listOriStone[i].isCanMine) {
                return true;
            }
        }
        return false;
    }
}
