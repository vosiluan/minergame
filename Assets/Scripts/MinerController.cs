﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody), typeof(CapsuleCollider))]
public class MinerController : MonoBehaviour
{
    [SerializeField] private Camera _camera = null;
    private Transform _cameraTransform = null;
    public Rigidbody _rigidbody;
    public FloatingJoystick _joystick;
    public Animator _animator;
    public float _moveSpeed;
    public float _mineSpeed;
    public QuarryController quarryController;
    public ParticleSystem upgradeEffect;
    public bool isInrange;
    private void Awake()
    {
        _cameraTransform = _camera.transform;
        upgradeEffect.Stop();
    }
    void FixedUpdate()
    {
        if (_joystick.Horizontal != 0 || _joystick.Vertical != 0)
        {
            Vector3 _vMovement = new Vector3(_joystick.Horizontal * _moveSpeed, 0, _joystick.Vertical * _moveSpeed);
            if (_vMovement != Vector3.zero)
            {
                _rigidbody.velocity = _vMovement;
                transform.rotation = Quaternion.LookRotation(_rigidbody.velocity, Vector3.up);
                _animator.SetBool("IsDig", false);
                _animator.SetFloat("Speed", 0.8f);
                _animator.speed = 1;
            }
        }
        else
        {
            if (quarryController != null)
            {
                if (quarryController.isCanMine)
                {
                    if (!_animator.GetBool("IsDig"))
                    {
                        _animator.SetBool("IsDig", isInrange);
                        _animator.SetFloat("Speed", 0f);
                        _animator.speed = _mineSpeed;
                    }
                }
                else
                {
                    _animator.SetBool("IsDig", false);
                    _animator.SetFloat("Speed", 0f);
                    _animator.speed = 1;
                }
            }
            else
            {
                _animator.SetFloat("Speed", 0f);
                _animator.speed = 1;
            }
        }
    }
    public void Mine()
    {
        if (quarryController != null)
        {
            if(quarryController.isCanMine)
                quarryController.MineStone();
        }
    }
    public void StopMove() {
        _animator.SetFloat("Speed", 0f);
        _animator.speed = 1;
    }
    public void SetDroper(QuarryController _quarry)
    {
        if (_quarry != null)
        {
            quarryController = _quarry;
            isInrange = true;
        }
        else
        {
            quarryController = null;
            isInrange = false;
        }
        _animator.SetBool("IsDig", isInrange);
        _animator.speed = _mineSpeed;
    }
    
}
