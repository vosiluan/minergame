using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using static GlobalEnum;

public class RoundBox_Controller : MonoBehaviour
{
    [SerializeField] int moneyUnlock;
    [SerializeField] Image imgProcess;
    [SerializeField] TMPro.TextMeshProUGUI txtMoney;
    [SerializeField] RoundBoxType boxType;
    [SerializeField] ParticleSystem unlockEffect;
    [SerializeField] BoxCollider boxCollider;
    [SerializeField] GameObject mainContent;
    [ShowIf("IsUnlockQuarry")] [SerializeField] GameObject quarryUnlock;
    [ShowIf("IsUnlockQuarry")] [SerializeField] GameObject roundBoxUnlockNext;
    [ShowIf("IsUnlockCart")] [SerializeField] GameObject cartGroup;
    [ShowIf("IsSellBox")] [SerializeField] List<GameObject> listPosSpawnMoney;
    [ShowIf("IsSellBox")] [SerializeField] GameObject moneyPrefab;
    public List<GameObject> listCurrentMoney;
    bool isUnlock;
    long currentMoneyUnlock;
    long moneyMinus;
    void OnEnable()
    {
        if(boxType != RoundBoxType.Sell && boxType != RoundBoxType.Upgrade)
        ResetBox();
        isUnlock = false;
        txtMoney.text = MyConstant.FormatToCurrency(moneyUnlock,999);
        listCurrentMoney = new List<GameObject>();
        currentMoneyUnlock = moneyUnlock;
    }
    public void ResetBox() {
        imgProcess.fillAmount = 0;
        unlockEffect.Stop();
        txtMoney.text = moneyUnlock.ToString();
    }
    void OnTriggerStay(Collider other)
    {
        if (!isUnlock) {
            if (other.gameObject.tag.Equals("Player"))
            {
                if (boxType == RoundBoxType.UnlockMap || boxType == RoundBoxType.UnlockQuarry || boxType == RoundBoxType.UnlockCart)
                {
                    if (imgProcess.fillAmount < 1)
                    {
                        
                        if (GameController.instance.player.myMoney < currentMoneyUnlock)
                        {
                            if ( GameController.instance.player.myMoney > 0) {
                                float _fillAmountOld = imgProcess.fillAmount;
                                imgProcess.fillAmount = imgProcess.fillAmount + Time.deltaTime;
                                int _money = (int)(moneyUnlock * imgProcess.fillAmount);
                                txtMoney.text = MyConstant.FormatToCurrency(moneyUnlock - _money, 999);
                                if (_money >= GameController.instance.player.myMoney) {
                                    _money = (int)GameController.instance.player.myMoney;
                                    imgProcess.fillAmount = ((float)_money / (float)moneyUnlock);
                                    currentMoneyUnlock = moneyUnlock - _money;
                                    txtMoney.text = MyConstant.FormatToCurrency(currentMoneyUnlock, 999);
                                    GameController.instance.player.myMoney -= _money;
                                    GameController.instance.player.SetTextMoney();
                                    Debug.Log("111111111111111");
                                    return;
                                }
                            }
                        }
                        else {
                            if (GameController.instance.player.myMoney <= 0)
                            {
                                return;
                            }
                            float _fillAmountOld = imgProcess.fillAmount;
                            imgProcess.fillAmount = imgProcess.fillAmount + Time.deltaTime;
                            int _money = (int)(moneyUnlock * imgProcess.fillAmount);
                            currentMoneyUnlock = moneyUnlock - _money;
                            moneyMinus = _money - (int)(_fillAmountOld * moneyUnlock);
                            txtMoney.text = MyConstant.FormatToCurrency(currentMoneyUnlock, 999);
                            GameController.instance.player.myMoney -= moneyMinus;
                            GameController.instance.player.SetTextMoney();
                            //long _playerMoney = GameController.instance.player.myMoney;
                            //GameController.instance.player.txtMoney.text = MyConstant.FormatToCurrency(_playerMoney - _money, 999);
                            Debug.Log("2222222222222222");
                        }
                    }
                    else
                    {
                        Debug.Log("333333333333333");
                        unlockEffect.Play();
                        HandleUnlock();
                    }
                }
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.Equals("Player"))
        {
            if (boxType == RoundBoxType.Upgrade)
            {
                GameController.instance.IsShowPanelUpgrade(true);
            }
            if (boxType == RoundBoxType.Sell)
            {
                MinerGoldController _miner = GameController.instance.miner.gameObject.GetComponent<MinerGoldController>();
                _miner.SellPosition = transform;
                _miner.Sell();
            }
        }
    }
    void OnTriggerExit(Collider other) {
        if (other.gameObject.tag.Equals("Player")) {
            //if (boxType == RoundBoxType.UnlockMap || boxType == RoundBoxType.UnlockQuarry || boxType == RoundBoxType.UnlockCart)
            //{
            //    if (moneyMinus>0) {
            //        GameController.instance.player.myMoney -= moneyMinus;
            //        DataManager.instance.userdata.money = GameController.instance.player.myMoney;
            //        moneyMinus = 0;
            //    }
            //}
            if (boxType == RoundBoxType.Upgrade) {
                GameController.instance.IsShowPanelUpgrade(false);
            }
        }
    }
    public void HandleUnlock() {
        isUnlock = true;
        if (boxType == RoundBoxType.UnlockQuarry)
        {
            /*Unclock Quarry*/
            quarryUnlock.SetActive(true);
            if(roundBoxUnlockNext)
                roundBoxUnlockNext.SetActive(true);
            DataManager.instance.userdata.unlockedBoxCount++;
            if (DataManager.instance.userdata.unlockedBoxCount == 3) {
                GameController.instance.StartAnimCameraToQuarry4();
            }
            /*Display Rock*/
        }
        if (boxType == RoundBoxType.UnlockMap) {
            /*Unclock new Map*/
            GameController.instance.UnlockMap2();
            DataManager.instance.userdata.isUnlockMap2 = true;
            /*Minus Money Player*/
        }
        if (boxType == RoundBoxType.UnlockCart)
        {
            /*Unclock new Map*/
            cartGroup.SetActive(true);
            Invoke("StartCart", 1f);
            DataManager.instance.userdata.unlockedBoxCount++;
            GameController.instance.StartAnimCameraToUpgradeBox();
        }
        //GameController.instance.player.myMoney -= moneyMinus;
        //moneyMinus = 0;
        //GameController.instance.player.SetTextMoney();
        DataManager.instance.userdata.money = GameController.instance.player.myMoney;
        mainContent.SetActive(false);
        boxCollider.enabled = false;
        Destroy(gameObject, 3f);
    }
    public void UnlockWhenLoadData() {
        if (boxType == RoundBoxType.UnlockQuarry)
        {
            quarryUnlock.SetActive(true);
            if (roundBoxUnlockNext)
                roundBoxUnlockNext.SetActive(true);
        }
        if (boxType == RoundBoxType.UnlockMap)
        {
            GameController.instance.UnlockMap2();
        }
        if (boxType == RoundBoxType.UnlockCart)
        {
            cartGroup.SetActive(true);
            Invoke("StartCart",1f);
        }
        mainContent.SetActive(false);
        boxCollider.enabled = false;
        Destroy(gameObject, 1f);
    }
    public void CreateMoney(int _index) {
        GameObject _money = Instantiate(moneyPrefab,listPosSpawnMoney[_index].transform);
        _money.transform.localPosition = new Vector3();
        listCurrentMoney.Add(_money);
    }
    public void ClearMoney() {
        foreach (GameObject _money in listCurrentMoney) {
            Destroy(_money);
        }
        listCurrentMoney.Clear();
    }
    bool IsUnlockQuarry() {
        return (boxType == RoundBoxType.UnlockQuarry) ? true : false;
    }
    bool IsUnlockCart()
    {
        return (boxType == RoundBoxType.UnlockCart) ? true : false;
    }
    bool IsSellBox()
    {
        return (boxType == RoundBoxType.Sell) ? true : false;
    }
    void StartCart() {
        GameController.instance.cartController.ResetCart();
    }
}
