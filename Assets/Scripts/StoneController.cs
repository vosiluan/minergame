using NaughtyAttributes;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GlobalEnum;
public class StoneController : MonoBehaviour
{
    [SerializeField] Rigidbody rig;
    [SerializeField] MeshCollider collider;
    [SerializeField] int force;
    public TypeStone typeStone;
    public bool isReady = false;
    public void InitStone() {
        rig.useGravity = false;
        collider.enabled = false;
    }
    public void MineStone() {
        rig.AddForce(transform.up * force);
        rig.useGravity = true;
        collider.enabled = true;
        GetReady();
    }
    public void GetReady()
    {
        Invoke("Ready",0.5f);
    }
    void Ready()
    {
        isReady = true;
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    if (!isReady) return;
    //    if (other.gameObject.GetComponent<MinerGoldController>() != null && !other.gameObject.GetComponent<MinerGoldController>().IsMax())
    //    {
    //        other.gameObject.GetComponent<MinerGoldController>().Add();
    //        Destroy(gameObject);
    //    }
    //}
    void OnCollisionEnter(Collision collision)
    {
        if (!isReady) return;
        if (collision.gameObject.GetComponent<MinerGoldController>() != null && !collision.gameObject.GetComponent<MinerGoldController>().IsMax())
        {
            collision.gameObject.GetComponent<MinerGoldController>().Add(typeStone);
            Destroy(gameObject);
        }
    }
}
