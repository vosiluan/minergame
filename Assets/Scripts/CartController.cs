using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartController : MonoBehaviour
{
    public List<GameObject> stoneSlots;
    public List<StackCargoController> stoneSlotSpaneds;
    public int MaxCapicity;
    public int currentCapicity = 0;
    public TMPro.TextMeshProUGUI txtCount;
    public SplineMover animMove;
    public Transform SellPosition;
    public bool isFull;
    void Start()
    {
        ResetCart();
    }
    public IEnumerator Add(List<StackCargoController> _listStackObj)
    {
        if (currentCapicity < MaxCapicity)
        {
            for (int i = 0; i < _listStackObj.Count; i++)
            {
                if (currentCapicity < MaxCapicity)
                {
                    _listStackObj[i].transform.parent = stoneSlots[currentCapicity].transform;
                    LeanTween.move(_listStackObj[i].gameObject, stoneSlots[currentCapicity].transform.position, 0.5f);
                    stoneSlotSpaneds.Add(_listStackObj[i]);
                    _listStackObj.RemoveAt(i);
                    currentCapicity++;
                    i--;
                    txtCount.text = currentCapicity.ToString() + "/" + MaxCapicity.ToString();
                    yield return new WaitForSeconds(0.1f);
                }
            }
            yield return new WaitForSeconds(1f);
            GameController.instance.miner.GetComponent<MinerGoldController>().ResetPosStack();
            if (currentCapicity == MaxCapicity) {
                animMove.StartMove(SplineMover.TypeMove.GoBack);
                isFull = true;
            }
        }
    }
    public void ResetCart() {
        currentCapicity = 0;
        txtCount.text = currentCapicity.ToString() + "/" + MaxCapicity.ToString();
        isFull = false;
        animMove.StartMove(SplineMover.TypeMove.GoForward);
    }
    public void Sell()
    {
        if (!isFull) return;
        StartCoroutine(SellGold());
    }
    IEnumerator SellGold()
    {
        int _totalMoney = 0;
        for (int i = stoneSlotSpaneds.Count - 1; i >= 0; i--)
        {
            stoneSlotSpaneds[i].transform.parent = null;
            LeanTween.move(stoneSlotSpaneds[i].gameObject, SellPosition.position, 1);
            _totalMoney += stoneSlotSpaneds[i].priceSell;
            yield return new WaitForSeconds(0.1f);
            SellGoldDone(i);
            Destroy(stoneSlotSpaneds[i].gameObject);
            
        }
        txtCount.text = "0/10";
        GameController.instance.boxSellController.gameObject.GetComponent<SellCener>().PushGold(_totalMoney);
        yield return new WaitForSeconds(2);
        stoneSlotSpaneds.Clear();
    }
    public void SellGoldDone(int _index)
    {
        Debug.Log("SellGold Done: "+_index);
        GameController.instance.boxSellController.CreateMoney(_index);
    }
}
