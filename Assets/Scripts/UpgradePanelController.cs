using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static UpgradeLevelCenter;
using static GlobalEnum;

public class UpgradePanelController : MonoBehaviour
{
    [SerializeField] TMPro.TextMeshProUGUI txtLevelMoveSpeed;
    [SerializeField] TMPro.TextMeshProUGUI txtLevelMineSpeed;
    [SerializeField] TMPro.TextMeshProUGUI txtLevelStackStone;

    [SerializeField] TMPro.TextMeshProUGUI txtMoneyMove;
    [SerializeField] TMPro.TextMeshProUGUI txtMoneyMine;
    [SerializeField] TMPro.TextMeshProUGUI txtMoneyStack;

    [Header("Button")]
    [SerializeField] Button btnUpgradeMove;
    [SerializeField] Button btnUpgradeMine;
    [SerializeField] Button btnUpgradeStack;

    [Header("CurrentLevelInfo")]
    [SerializeField] LevelInfo currentlevelMove;
    [SerializeField] LevelInfo currentlevelMine;
    [SerializeField] LevelInfo currentlevelStack;

    public UpgradeLevelCenter upgradeLevelCenter;
    public void BtnCLose() {
        gameObject.SetActive(false);
        GameController.instance.miner._joystick.enabled = true;
        GameController.instance.miner._joystick.gameObject.SetActive(true);
    }
    public void BtnUpdateMoveSpeed() {
        if (GameController.instance.player.myMoney < currentlevelMove.moneyUnlockLevelNext) return;
        if (upgradeLevelCenter.CheckLevel(currentlevelMove.levelNumber + 1)) {
            GameController.instance.player.myMoney -= currentlevelMove.moneyUnlockLevelNext;
            GameController.instance.player.SetTextMoney();
            DataManager.instance.userdata.money = GameController.instance.player.myMoney;

            DataManager.instance.userdata.levelMoveSpeed++;
            LoadLevel(TypeUpgrade.MoveSpeed);
            GameController.instance.miner._moveSpeed = currentlevelMove.levelValue;
            GameController.instance.miner.upgradeEffect.Play();
        }
    }
    public void BtnUpdateMineSpeed()
    {
        if (GameController.instance.player.myMoney < currentlevelMine.moneyUnlockLevelNext) return;
        if (upgradeLevelCenter.CheckLevel(currentlevelMine.levelNumber+1))
        {
            GameController.instance.player.myMoney -= currentlevelMine.moneyUnlockLevelNext;
            GameController.instance.player.SetTextMoney();
            DataManager.instance.userdata.money = GameController.instance.player.myMoney;

            DataManager.instance.userdata.levelMineSpeed++;
            LoadLevel(TypeUpgrade.MineSpeed);
            GameController.instance.miner._mineSpeed = currentlevelMine.levelValue;
            GameController.instance.miner.upgradeEffect.Play();
        }
    }
    public void BtnUpdateStackStone()
    {
        if (GameController.instance.player.myMoney < currentlevelStack.moneyUnlockLevelNext) return;
        if (upgradeLevelCenter.CheckLevel(currentlevelStack.levelNumber + 1))
        {
            GameController.instance.player.myMoney -= currentlevelStack.moneyUnlockLevelNext;
            GameController.instance.player.SetTextMoney();
            DataManager.instance.userdata.money = GameController.instance.player.myMoney;

            DataManager.instance.userdata.levelStackStone++;
            LoadLevel(TypeUpgrade.StackStone);
            GameController.instance.miner.GetComponent<MinerGoldController>().MaxCapicity = (int)currentlevelStack.levelValue;
            GameController.instance.miner.upgradeEffect.Play();
        }
    }
    public void LoadData() {
        LoadLevel(TypeUpgrade.MoveSpeed);
        LoadLevel(TypeUpgrade.MineSpeed);
        LoadLevel(TypeUpgrade.StackStone);
    }
    void LoadLevel(TypeUpgrade _typeUpgrade) {
        switch (_typeUpgrade) {
            case TypeUpgrade.MoveSpeed:
                LevelInfo _levelInfoMove = upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.MoveSpeed, DataManager.instance.userdata.levelMoveSpeed);
                txtLevelMoveSpeed.text = _levelInfoMove.levelName;
                if (_levelInfoMove.levelNumber == upgradeLevelCenter.levelMaxNumber) {
                    txtMoneyMove.text = "Max";
                    btnUpgradeMove.interactable = false;
                }
                else
                    txtMoneyMove.text = _levelInfoMove.moneyUnlockLevelNext.ToString();
                currentlevelMove = _levelInfoMove;
                break;
            case TypeUpgrade.MineSpeed:
                LevelInfo _levelInfoMine = upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.MineSpeed, DataManager.instance.userdata.levelMineSpeed);
                txtLevelMineSpeed.text = _levelInfoMine.levelName;
                if (_levelInfoMine.levelNumber == upgradeLevelCenter.levelMaxNumber)
                {
                    txtMoneyMine.text = "Max";
                    btnUpgradeMine.interactable = false;
                }
                else
                    txtMoneyMine.text = _levelInfoMine.moneyUnlockLevelNext.ToString();
                currentlevelMine = _levelInfoMine;
                break;
            case TypeUpgrade.StackStone:
                LevelInfo _levelInfoStack = upgradeLevelCenter.GetLevelInfoMove(TypeUpgrade.StackStone, DataManager.instance.userdata.levelStackStone);
                txtLevelStackStone.text = _levelInfoStack.levelName;
                if (_levelInfoStack.levelNumber == upgradeLevelCenter.levelMaxNumber)
                {
                    txtMoneyStack.text = "Max";
                    btnUpgradeStack.interactable = false;
                }
                else
                    txtMoneyStack.text = _levelInfoStack.moneyUnlockLevelNext.ToString();
                currentlevelStack = _levelInfoStack;
                break;
        }
    }
}
