using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoxCartSellController : MonoBehaviour
{
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent<CartController>()!=null) {
            other.gameObject.GetComponent<CartController>().Sell();
        }
    }
}
