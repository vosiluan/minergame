﻿using System.Collections;
using System.Collections.Generic;
using BayatGames.SaveGameFree;
using BayatGames.SaveGameFree.Serializers;
using UnityEngine;

public class DataManager
{
	public static DataManager instance
	{
		get
		{
			return ins;
		}
	}
	private static DataManager ins;

	public DataManager() { }

	#region Init / Save / Load / Clear
	public ISaveGameSerializer activeSerializer
	{
		get
		{
			if (m_ActiveSerializer == null)
			{
				m_ActiveSerializer = new SaveGameJsonSerializer();
			}
			return m_ActiveSerializer;
		}
	}
	private ISaveGameSerializer m_ActiveSerializer;
	public static void Init()
	{
		if (ins != null)
		{
#if TEST
			Debug.LogError("Đã Init DataManager rồi");
#endif
			return;
		}
		SaveGame.Encode = true;
		SaveGame.EncodePassword = "Acazia";
		// --- Clear Data --- //
		//		QuickSaveRoot.Delete (MyConstant.rootSaveName);
		// ------------------ //
		if (SaveGame.Exists(MyConstant.rootSaveName))
		{
#if TEST
			Debug.Log (">>> Load DataManager <<<");
#endif
			LoadData();
		}
		else
		{
#if TEST
			Debug.Log (">>> Create New DataManager <<<");
#endif
			SaveData();
		}
	}
	public static void SaveData()
	{
		if (ins == null)
		{
			ins = new DataManager();
		}
		if (ins.versionData != ins.save_versionData)
		{ // khi save lỗi
			Debug.LogError("DataManager version: " + ins.versionData + " - " + ins.save_versionData);
			return;
		}
		SaveGame.Save<DataManager>(MyConstant.rootSaveName, ins, ins.activeSerializer);
		if (ins.versionData == 0)
		{
			ins.versionData = 1;
			ins.save_versionData = 1;
		}
#if TEST
		string _json = JsonUtility.ToJson(ins);
		Debug.Log (">>> Save Data : " + _json + "| userData: "+ ins.userdata.ToString());
		string _jsonUserData = JsonUtility.ToJson(ins.userdata);
		Debug.Log(">>> UserData: "+ _jsonUserData);
#endif
	}
	public static void LoadData()
	{
		if (ins == null)
		{
			ins = new DataManager();
		}

		try
		{
			ins = SaveGame.Load<DataManager>(
				MyConstant.rootSaveName,
				new DataManager(),
				ins.activeSerializer);
		}
		catch
		{
			ClearData();
		}

		if (ins.versionData != ins.save_versionData)
		{
			ins.versionData = ins.save_versionData;
		}
#if TEST
		string _json = JsonUtility.ToJson(ins);
		Debug.Log (">>> Load Data : " + _json );
		string _jsonUserData = JsonUtility.ToJson(ins.userdata);
		Debug.Log(">>> UserData: "+ _jsonUserData);
#endif
    }
    public static void ClearData()
	{
		ins = new DataManager();
		SaveGame.Delete(MyConstant.rootSaveName);
		ins.versionData = 0;
		ins.save_versionData = -1;
		SaveData();
	}
#endregion

#region Serializable Data
	///<summary>
	/// save_versionData : biến tạo ra để tham số tới versionData dùng để backup data khi save lỗi
	///		- -1: chưa tạo biến 
	///		- 0: mới tạo biến
	///		- 1: đã tạo biến
	///</summary>
	public int save_versionData
	{
		get
		{
			if (_save_versionData == -1)
			{
				_save_versionData = PlayerPrefs.GetInt(MyConstant.save_kVersionDataName, 0);
				if (_save_versionData == -1)
				{
					_save_versionData = 0;
				}
				PlayerPrefs.SetInt(MyConstant.save_kVersionDataName, _save_versionData);
			}
			return _save_versionData;
		}
		set
		{
			_save_versionData = value;
			PlayerPrefs.SetInt(MyConstant.save_kVersionDataName, _save_versionData);
		}
	}
	private int _save_versionData = -1;
	public int versionData = 0;
	public int sfxStatus
	{ // trạng thái tắt bật âm thanh hiệu ứng (0: tắt, 1 : bật)
		get
		{
			if (_sfxStatus == -1)
			{
				_sfxStatus = PlayerPrefs.GetInt(MyConstant.save_kSfxName, 1);
			}
			return _sfxStatus;
		}
		set
		{
			_sfxStatus = value;
			PlayerPrefs.SetInt(MyConstant.save_kSfxName, _sfxStatus);
		}
	}
	private int _sfxStatus = -1;

	public int musicStatus
	{ // trạng thái tắt bật nhạc nền (0: tắt, 1 : bật)
		get
		{
			if (_musicStatus == -1)
			{
				_musicStatus = PlayerPrefs.GetInt(MyConstant.save_kMusicName, 1);
			}
			return _musicStatus;
		}
		set
		{
			_musicStatus = value;
			PlayerPrefs.SetInt(MyConstant.save_kMusicName, _musicStatus);
		}
	}
	private int _musicStatus = -1;

	public int vibrationStatus
	{ // trạng thái tắt bật hiệu ứng rung (0: tắt, 1 : bật)
		get
		{
			if (_vibrationStatus == -1)
			{
				_vibrationStatus = PlayerPrefs.GetInt(MyConstant.save_kVibrationName, 1);
			}
			return _vibrationStatus;
		}
		set
		{
			_vibrationStatus = value;
			PlayerPrefs.SetInt(MyConstant.save_kVibrationName, _vibrationStatus);
		}
	}
	private int _vibrationStatus = -1;
#endregion
#region Non Serializable Data
	// [System.NonSerialized] public bool hadSendJoinRoom;
	[System.NonSerialized] public bool isNewUser; // user mới đăng ký, nếu = true thì show hiệu ứng + tiền lúc vào trang login
	[System.NonSerialized] public long goldRewardIfNewUser;
#endregion
	public class UserData {
		public long money;
		public bool isUnlockMap2;
		public int unlockedBoxCount;
		public int levelMoveSpeed;
		public int levelMineSpeed;
		public int levelStackStone;
		public UserData(){
			money = 600;
			isUnlockMap2 = false;
			unlockedBoxCount = 0;
			levelMoveSpeed = 1;
			levelMineSpeed = 1;
			levelStackStone = 1;
		}
	}
	public UserData userdata
	{
		get
		{
			if (_userData == null)
			{
#if TEST
				Debug.Log ("--- Create New UserData! ---");
#endif
				_userData = new UserData();
			}
			return _userData;
		}
		set
		{
			_userData = value;
		}
	}
	[SerializeField] private UserData _userData;
}
