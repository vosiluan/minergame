using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class IntroScene_Controller : MonoBehaviour
{
    [SerializeField] VideoPlayer videoIntro;
    void Awake(){
        videoIntro.Play();
        StartCoroutine(WaitToChangeScene());
    }
    IEnumerator WaitToChangeScene() {
        yield return new WaitForSeconds(13.5f);
        SceneLoaderManager.instance.LoadScene(SceneIndex.LoginScene);
    }
}
