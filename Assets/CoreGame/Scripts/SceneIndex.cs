﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SceneIndex
{
    IntroScene = 0,
    LoginScene =1,
    LobbyScene = 2,
    IngameScene = 3,
    Unknown = 999
}
