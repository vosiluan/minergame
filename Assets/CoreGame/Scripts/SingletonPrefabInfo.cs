﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "SingletonPrefabInfo", menuName="GameInfo/SingletonPrefabInfo")]
public class SingletonPrefabInfo : ScriptableObject {
    public PopupManager popupManagerPrefab;
    public LoadingCanvasController loadingCanvasPrefab;
    public SceneLoaderManager sceneLoaderManagerPrefab;
    public MyAudioManager myAudioManagerPrefab;

    [System.Serializable] 
    public class GlobalAudioInfo{
        public AudioClip BGM_Login;
        public AudioClip BGM_Lobby;
        public AudioClip BGM_Map;
        public AudioClip BGM_Ingame1;
        public AudioClip BGM_Ingame2;
        public AudioClip BGM_Ingame3;
        public AudioClip sfx_ClickButton;
        public AudioClip sfx_Popup;
        public AudioClip sfx_Hit;
        public AudioClip sfx_Hurt;
        public AudioClip sfx_Lose;
        public AudioClip sfx_Win;
    }
    public GlobalAudioInfo globalAudioInfo;
}