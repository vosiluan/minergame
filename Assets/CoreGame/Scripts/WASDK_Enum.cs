﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WASDK_Enum 
{
    public enum PanelState { 
        Hide =0,
        Show =1
    }
    public enum LoginContentUI { 
        Login,
        Register,
        ForgotPW
    }
}
