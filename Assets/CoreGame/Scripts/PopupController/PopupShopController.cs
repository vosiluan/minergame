﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupShopController : IPopupController
{
	public override void ResetData()
	{
		base.ResetData();
	}
	public void Init(System.Action _onClose = null)
	{
		/*Init data shop lại*/
		onClose = _onClose;
		CoreGameManager.instance.RegisterNewCallbackPressBackKey(OnBtnCloseClicked);
	}
	#region On Button Clicked
	public void OnBtnCloseClicked()
	{
		Hide(() => {
			Close();
		});
	}
	public override void Close()
	{
		if (onClose != null)
		{
			onClose();
			onClose = null;
		}
		SelfDestruction();
	}
	#endregion
}
